pub const METERS_PER_AU: f64 = 149_597_870_700f64;

/// Keplerian orbital elements
#[derive(Clone, Copy, Debug)]
pub struct KeplerElements {
    /// Semi-major axis in meters
    pub a: f64,
    /// Eccentricity
    pub e: f64,
    /// Inclination in radians
    pub i: f64,
    /// Longitude of ascending node in radians
    pub o: f64,
    /// Argument of periapsis in radians
    pub w: f64,
}

impl KeplerElements {
    pub fn apoapsis(&self) -> f64 {
        (1.0 + self.e) * self.a
    }

    pub fn periapsis(&self) -> f64 {
        (1.0 - self.e) * self.a
    }

    pub fn semilatus_rectum(&self) -> f64 {
        // l = b^2/a
        // l = sqrt(rp * ra)^2/a
        // l = (rp * ra)/a
        // l = (((1.0 - e) * a) * ((1.0 + e) * a)) / a
        // l = (1.0 - e) * a * (1.0 + e) * a / a
        // l = (1.0 - e) * a * (1.0 + e)
        // l = (1.0 - e) * (1.0 + e) * a
        // (1.0 - self.e) * (1.0 + self.e) * self.a
        self.a * (1.0 - self.e.powi(2))
    }

    pub fn semiminor_axis(&self) -> f64 {
        self.semilatus_rectum() / (1.0 - self.e.powi(2)).sqrt()
    }
}

impl From<vsop87::KeplerianElements> for KeplerElements {
    fn from(keplerian_elements: vsop87::KeplerianElements) -> KeplerElements {
        KeplerElements {
            a: keplerian_elements.semimajor_axis() * METERS_PER_AU,
            e: keplerian_elements.eccentricity(),
            i: keplerian_elements.inclination(),
            o: keplerian_elements.ascending_node(),
            w: keplerian_elements.periapsis(),
        }
    }
}

impl From<vsop87::VSOP87Elements> for KeplerElements {
    fn from(vsop_elements: vsop87::VSOP87Elements) -> KeplerElements {
        let vsop_kepler_elements: vsop87::KeplerianElements = vsop_elements.into();
        vsop_kepler_elements.into()
    }
}