use crate::elements::{KeplerElements, METERS_PER_AU};
use nalgebra::{Matrix3, Matrix4, Rotation3, Vector2, Vector3};
use svg::node::element::Circle;

fn matrix3_to_svg_matrix(a: Matrix3<f64>) -> String {
    format!(
        "matrix({} {} {} {} {} {})",
        a[(0, 0)],
        a[(1, 0)],
        a[(0, 1)],
        a[(1, 1)],
        a[(0, 2)],
        a[(1, 2)]
    )
}

fn rotate_to_axis(dir: &Vector3<f64>, up: &Vector3<f64>) -> Rotation3<f64> {
    let reaxis: Rotation3<f64> = Rotation3::rotation_between(dir, &Vector3::z_axis()).unwrap();

    let reaxised_up = reaxis.transform_vector(up);

    let orient_up = if reaxised_up.cross(&Vector3::y_axis()) == Vector3::new(0.0, 0.0, 0.0) {
        if dbg!(reaxised_up) == dbg!(Vector3::y_axis().into_inner()) {
            Rotation3::identity()
        } else {
            Rotation3::from_axis_angle(&Vector3::z_axis(), std::f64::consts::PI)
        }
    } else {
        Rotation3::rotation_between(&reaxis.transform_vector(up), &Vector3::y_axis()).unwrap()
    };

    orient_up * reaxis
}

fn slice_matrix(a: Matrix4<f64>) -> Matrix3<f64> {
    // let pre_rotation: Rotation3<f64> = Rotation3::face_towards(
    //     &Vector3::z_axis(),
    //     &Vector3::x_axis(),
    // );
    // let pre_rotation: Rotation3<f64> =
    //     Rotation3::from_axis_angle(&Vector3::z_axis(), 2.0 * std::f64::consts::PI / 2.0)
    //         * Rotation3::rotation_between(&Vector3::x_axis(), &Vector3::z_axis()).unwrap();

    let pre_rotation = rotate_to_axis(&Vector3::z_axis(), &Vector3::y_axis());

    let a: Matrix4<f64> = pre_rotation.to_homogeneous() * a;

    Matrix3::new(
        a[(0, 0)],
        a[(0, 1)],
        a[(0, 3)],
        a[(1, 0)],
        a[(1, 1)],
        a[(1, 3)],
        0.0,
        0.0,
        1.0,
    )
}

fn to_svg_matrix(input: Matrix4<f64>) -> String {
    let sliced_matrix: Matrix3<f64> = slice_matrix(input);

    let zoom_level: f64 = 10.0;
    let zoom: Matrix3<f64> = Matrix3::new_scaling(zoom_level / METERS_PER_AU);
    let centering: Matrix3<f64> = Matrix3::new_translation(&Vector2::new(400.0, 400.0));

    matrix3_to_svg_matrix(centering * zoom * sliced_matrix)
}

pub fn elements_to_circle(elements: KeplerElements, color: &'static str) -> Circle {
    let ascension_node_rotation = Rotation3::from_axis_angle(&Vector3::z_axis(), elements.o);
    let inclination_rotation = Rotation3::from_axis_angle(&Vector3::y_axis(), elements.i);
    let periapsis_rotation = Rotation3::from_axis_angle(&Vector3::z_axis(), elements.w);

    let total_rotation: Matrix4<f64> =
        (ascension_node_rotation * inclination_rotation * periapsis_rotation).to_homogeneous();

    let focus_transformation: Matrix4<f64> =
        Matrix4::new_translation(&Vector3::new((elements.periapsis() - elements.a), 0.0, 0.0));

    let axes_transformation: Matrix4<f64> =
        Matrix4::new_nonuniform_scaling(&Vector3::new(elements.a, elements.semiminor_axis(), 1.0));

    Circle::new()
        .set("r", 1)
        .set("fill", "none")
        .set("stroke", color)
        .set("stroke-width", 2)
        .set("vector-effect", "non-scaling-stroke")
        .set(
            "transform",
            to_svg_matrix(total_rotation * focus_transformation * axes_transformation),
        )
}
